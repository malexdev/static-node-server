static-node-server
==================

A simple static node server

Deployment is simple: 

1. Download/clone this project
1. Run `npm install`
1. Put your files in `/public`
1. Change your port in `settings.json`
1. Run `node app` (for production, I use [`forever`](https://www.npmjs.org/package/forever))